<?php
class Api_model extends CI_Model{

	public function user_auth($username, $password){

		return TRUE;
		// $q = $this->db->get_where('tblusers',array('UserName' => $username, 'UserPassword' => $password));
		// if($q->result_array() != null){
		// 	return $q->result_array();
		// }else{
		// 	return null;
		// } 
	}

//============================= USERS DB QUERY ==========================================================//

	public function get_users($id=NULL, $sub_id=1){
		if($id === NULL){
			$sql = "SELECT u.user_id as id, p.firstname as user_fname, p.lastname  as user_lname, p.email as user_email_address, u.`password` as user_password, u.username as user_username from compassdb.user_profile u inner join compassdb.person p on p.person_id = u.person_id";
			$q = $this->db->query($sql);

			if($q->num_rows() > 0){
				foreach($q->result_array() as $row){
					$data[] = $row;
				}
				return $data;
			}else{
				return 0;
			}

		}else{
			$sql = "SELECT u.user_id as id, p.firstname as user_fname, p.lastname  as user_lname, p.email as user_email_address, u.`password` as user_password, u.username as user_username from compassdb.user_profile u inner join compassdb.person p on p.person_id = u.person_id where p.subscription_id = $sub_id and u.user_id = $id";
			$q = $this->db->query($sql);

			if($q->num_rows() > 0){
				foreach($q->result_array() as $key => $row){
					if($key == 0){
						return $row;
					}
				}
			}else{
				return NULL;
			}	
		}

	}
	
	public function add_user($data, $sub_id=1){
		$pdata = array(
					'firstname' => $data['user_fname'],
					'lastname' 	=> $data['user_lname'],
					'email' 	=> $data['user_email_address'],
					'subscription_id' => $sub_id
				);

		if($this->db->insert('person',$pdata)){
		 	$newId = $this->db->insert_id();

		 		$udata = array(
					'username' => $data['user_username'],
					'password' => $data['user_password'],
					'person_id' => $newId
				);

				$this->db->insert('user_profile',$udata);
				return "success";
		}else{
		 	return NULL;
		}
	}
	
	public function update_user($id, $data, $sub_id=1){
		$pdata = array(
					'firstname' => $data['user_fname'],
					'lastname' 	=> $data['user_lname'],
					'email' 	=> $data['user_email_address'],
					'subscription_id' => $sub_id
				);

 		$udata = array(
			'username' => $data['user_username'],
			'password' => $data['user_password']
		);

 		if($pdata){
			$this->db->where('person_id',$id);
			$this->db->update('person',$pdata);
		}	

 		if($udata){
			$this->db->where('person_id',$id);
			$this->db->update('user_profile',$udata);
		}	

		return "success";
	}	
	
	public function delete_user($id, $stat){
		//$this->db->delete('tablename', array('ID' => $id));
		return NULL;
	}	

//============================= UPLOAD ==========================================================//
  
  public function upload($new_name, $path = 'uploads/'){

    $config['upload_path']          = 'public/' . $path;
    $config['allowed_types']        = 'gif|jpg|png|svg';
    $config['max_size']             = 10000;
    $config['overwrite']             = true;
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;
    $config['file_name'] = $new_name;

    
    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('file'))
    {
            // $error = array('error' => $this->upload->display_errors());
            // var_dump($error);
            // exit;
            return null;
    }
    else
    {
            $data = array('upload_data' => $this->upload->data());
            return $data;
    }
    
  }

  public function upload2($new_name2, $path2 = 'uploads/'){

    $config2['upload_path']          = 'public/' . $path2;
    $config2['allowed_types']        = 'gif|jpg|png|svg';
    $config2['max_size']             = 10000;
    $config2['overwrite']             = true;
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;
    $config2['file_name'] = $new_name2;

    var_dump($config2);

    $this->load->library('upload');
    $this->upload->initialize($config2);

    if ( ! $this->upload->do_upload('file2'))
    {
            // $error = array('error' => $this->upload->display_errors());
            // var_dump($error);
            // exit;
            return null;
    }
    else
    {
            $data = array('upload_data' => $this->upload->data());
            return $data;
    }
    
  }

//============================= STORES DB QUERY ==========================================================//

	public function get_stores($id=NULL, $where=NULL, $sort=NULL, $limit = 10, $skip = 0, $sub_id=1){

		if($id === NULL){

      $where_s = "";
      if($where){

        $whereparams = json_decode($where);

        // var_dump($whereparams);

        if($whereparams){
          foreach($whereparams as $paramkey=>$param){
            foreach($param as $key=>$value){
              $where_s .= " AND " . $paramkey . " " . $key . " '" . $value . "'";
            }
          }
        }
        // var_dump($whereparams);
        // echo $where_s;
        // exit;
      }

			// $sql_info = "SELECT store_id as id, store_name, contact_number as store_contact_number, email_address as store_email_address, website as store_website, description as store_description, store_trading_hours, store_trading_hours_custom, store_trading_hours_daily, store_tags, owner_name, owner_email, owner_contact_number, is_active, is_featured as store_featured, eventbrite_id, store_image, date_created AS date_created from compassdb.store_info where sub_id = $sub_id " . (($where) ? $where_s : "") . " AND is_active = 1 " . (($sort) ? "ORDER BY $sort" : "") . " LIMIT $limit OFFSET $skip";

      $sql_info = "SELECT s.store_id as id, s.store_name, s.contact_number as store_contact_number, s.email_address as store_email_address, s.website as store_website, s.description as store_description, s.store_trading_hours, s.store_trading_hours_custom, s.store_trading_hours_daily, s.store_tags, s.owner_name, s.owner_email, s.owner_contact_number, s.is_active, s.is_featured as store_featured, s.eventbrite_id, s.store_image, s.date_created AS date_created, c.category_id from compassdb.store_info s LEFT JOIN category_links c ON s.store_id = c.ref_key where sub_id = 1 AND is_active = 1" . (($where) ? $where_s : "") . " GROUP BY (s.store_id)" . (($sort) ? "ORDER BY $sort" : "") . " LIMIT $limit OFFSET $skip";

      // echo $sql_info;
      // exit;

			$q_info = $this->db->query($sql_info);

      $sql_info_count = "SELECT COUNT(*) as c from (SELECT COUNT(DISTINCT s.store_id) as c from compassdb.store_info s LEFT JOIN category_links c ON s.store_id = c.ref_key where sub_id = 1 AND is_active = 1" . (($where) ? $where_s : "") . " GROUP BY (s.store_id)" . ") c";

      // echo $sql_info_count;
      // exit;
			$q_info_count = $this->db->query($sql_info_count);

			if($q_info->num_rows() > 0){
				foreach($q_info->result_array() as $row){
					$stores_info[] = $row;
				}
				
					
				foreach ($stores_info as $key => $store_info) {

					$store_categories = NULL;
					$store_tags = NULL;
					$store_attach = NULL;
					$store_hrs = NULL;

          if($store_info['store_tags']){
            $stores_info[$key]['store_tags'] = json_decode($store_info['store_tags']);
          }

          if($store_info['store_trading_hours_daily']){
            $stores_info[$key]['store_trading_hours_daily'] = json_decode($store_info['store_trading_hours_daily']);
          }

          if($store_info['store_trading_hours_custom']){
            $stores_info[$key]['store_trading_hours_custom'] = json_decode($store_info['store_trading_hours_custom']);
          }

					$sql_categories = "SELECT c.category_id, c.item_name as category_name, c.category_image, c.category_logo, c.category_image FROM compassdb.category_links cl INNER JOIN compassdb.categories c ON cl.category_id = c.category_id WHERE cl.table_id = get_table_id('store_info') AND cl.ref_key = ". $store_info['id'];
					$q_categories = $this->db->query($sql_categories);

					if($q_categories->num_rows() > 0){
						foreach($q_categories->result_array() as $row){
							$store_categories[] = $row;
						}
						$stores_info[$key]['store_category'] = $store_categories;	
					}

					// $sql_tags = "SELECT t.tag_id, t.item_name AS tag_name FROM compassdb.tag_links tl INNER JOIN compassdb.tags t on tl.tag_id = t.tag_id WHERE tl.table_id = get_table_id('store_info') AND tl.ref_key = ". $store_info['id'];
					// $q_tags = $this->db->query($sql_tags);

					// if($q_tags->num_rows() > 0){
					// 	foreach($q_tags->result_array() as $row){
					// 		$store_tags[] = $row;
					// 	}	
					// 	$stores_info[$key]['store_tags'] = $store_tags;

					// }

					$sql_attach = "SELECT a.attachment_id, a.media AS media_url, a.media_name, (SELECT type_name FROM compassdb.media_type m WHERE m.media_type_id = a.media_type_id) AS media_type FROM compassdb.media_attachments a WHERE a.sub_id = 1 AND a.table_id = get_table_id('store_info') AND ref_key = ". $store_info['id'];

					$q_attach = $this->db->query($sql_attach);

					if($q_attach->num_rows() > 0){
						foreach($q_attach->result_array() as $row){
							$store_attach[] = $row;
						}		
						$stores_info[$key]['store_attachments'] = $store_attach;									
					}


					// $sql_hrs = "SELECT d.schedule_day_id AS id, (SELECT day_name FROM z_day x WHERE d.day_id = x.day_id) AS `day`, d.time_start AS opening_time, d.time_end AS closing_time FROM schedule_day d WHERE d.schedule_id = (SELECT x.schedule_id FROM schedule_info x WHERE x.sub_id = 1 AND  x.table_id = get_table_id('store_info') AND x.ref_key =". $store_info['id'] .")";

					// $q_hrs = $this->db->query($sql_hrs);

					// if($q_hrs->num_rows() > 0){
					// 	foreach($q_hrs->result_array() as $row){
					// 		$store_hrs[] = $row;
					// 	}	
					// 	$stores_info[$key]['store_hours'] = $store_hrs;						
					// }

				}
					
				//var_dump($stores_info);	
				return array('total_rows' => $q_info_count->result_array()[0]['c'], 'stores' => $stores_info);

			}else{
				return array('total_rows' => 0, 'stores' => array());
			}

		}else{
			$sql = "SELECT store_id AS id, store_name, contact_number AS store_contact_number, email_address AS store_email_address, website AS store_website, description AS store_description, store_trading_hours, store_trading_hours_custom, store_trading_hours_daily, store_tags, owner_name, owner_email, owner_contact_number, is_active, is_featured AS store_featured, eventbrite_id, store_image, date_created AS date_created FROM compassdb.store_info WHERE store_id = $id AND sub_id = $sub_id";
			$q = $this->db->query($sql);

			if($q->num_rows() > 0){
				foreach($q->result_array() as $key => $row){
					if($key == 0){
						$store_info = $row;
					}
				}


				if($store_info){


					$sql_categories = "SELECT c.category_id, c.item_name AS category_name, c.category_image FROM compassdb.category_links cl INNER JOIN compassdb.categories c ON cl.category_id = c.category_id WHERE cl.table_id = get_table_id('store_info') AND cl.ref_key = ". $store_info['id'];
					$q_categories = $this->db->query($sql_categories);

					if($q_categories->num_rows() > 0){
						foreach($q_categories->result_array() as $row){
							$store_categories[] = $row;
						}
						$store_info['store_category'] = $store_categories;
					}		
				
					//var_dump($store_info);

          if($store_info['store_tags']){
            $store_info['store_tags'] = json_decode($store_info['store_tags']);
          }

          if($store_info['store_trading_hours_daily']){
            $store_info['store_trading_hours_daily'] = json_decode($store_info['store_trading_hours_daily']);
          }

          if($store_info['store_trading_hours_custom']){
            $store_info['store_trading_hours_custom'] = json_decode($store_info['store_trading_hours_custom']);
          }

					// $sql_tags = "SELECT t.tag_id, t.item_name AS tag_name FROM compassdb.tag_links tl INNER JOIN compassdb.tags t on tl.tag_id = t.tag_id WHERE tl.table_id = get_table_id('store_info') AND tl.ref_key = ". $store_info['id'];
					// $q_tags = $this->db->query($sql_tags);

					// if($q_tags->num_rows() > 0){
					// 	foreach($q_tags->result_array() as $row){
					// 		$store_tags[] = $row;
					// 	}	
					// 	$store_info['store_tags'] = $store_tags;

					// }

					$sql_attach = "SELECT a.attachment_id, a.media AS media_url, a.media_name, (SELECT type_name FROM compassdb.media_type m WHERE m.media_type_id = a.media_type_id) AS media_type FROM compassdb.media_attachments a WHERE a.sub_id = 1 AND a.table_id = get_table_id('store_info') AND ref_key = ". $store_info['id'];

					$q_attach = $this->db->query($sql_attach);

					if($q_attach->num_rows() > 0){
						foreach($q_attach->result_array() as $row){
							$store_attach[] = $row;
						}		
						$store_info['store_attachments'] = $store_attach;									
					}


					// $sql_hrs = "SELECT d.schedule_day_id AS id, (SELECT day_name FROM z_day x WHERE d.day_id = x.day_id) AS `day`, d.time_start AS opening_time, d.time_end AS closing_time FROM schedule_day d WHERE d.schedule_id = (SELECT x.schedule_id FROM schedule_info x WHERE x.sub_id = 1 AND  x.table_id = get_table_id('store_info') AND x.ref_key =". $store_info['id'] .")";

					// $q_hrs = $this->db->query($sql_hrs);

					// if($q_hrs->num_rows() > 0){
					// 	foreach($q_hrs->result_array() as $row){
					// 		$store_hrs[] = $row;
					// 	}	
					// 	$store_info['store_hours'] = $store_hrs;						
					// }
          
					return $store_info;

				}else{
					return NULL;
				}


			}else{
				return NULL;
			}	
		}
	}
	
	public function add_store($data, $sub_id=1){

		$infodata = array(
			'store_name' 		=> $data['store_name'],
			'contact_number' 	=> $data['store_contact_number'],
			'email_address' 	=> $data['store_email_address'],
			'website' 			=> $data['store_website'],
			'description' 		=> $data['store_description'],
			'store_trading_hours' 		=> $data['store_trading_hours'],
			'store_trading_hours_daily' 		=> json_encode($data['store_trading_hours_daily']),
			'store_trading_hours_custom' 		=> json_encode($data['store_trading_hours_custom']),
			'store_tags' 		=> json_encode($data['store_tags']),
			'owner_name'	 	=> $data['owner_name'],
			'owner_email' 		=> $data['owner_email'],
			'owner_contact_number' => $data['owner_contact_number'],
			'is_active' 		=> $data['is_active'],
			'is_featured' 		=> $data['store_featured'],
			'has_media' 		=> 1,
			'sub_id' 			=> $sub_id
		);

		if($this->db->insert('store_info',$infodata)){
		 	$newId = $this->db->insert_id();

       // Upload file
       $uploaddata = $this->upload($newId, 'uploads/stores/');

       if($uploaddata){
         $updateimagedata = array(
			      'store_image' 		=> 'uploads/stores/' . $uploaddata['upload_data']['file_name']
         );

         $this->db->where('store_id',$newId);
         if($this->db->update('store_info',$updateimagedata)){
         }
       }

		 		// Adding Store Categories
		 		echo "Category Counter" . count($data['store_category']);
		 		if(is_array($data['store_category'])){
		 			foreach ($data['store_category'] as $key => $category) {
						$add_cat_query = "INSERT INTO category_links SET ref_key = $newId, table_id = get_table_id('store_info'), category_id = $category";
						$q_add_cat_query = $this->db->query($add_cat_query);
		 			}

		 		}else{
					
					$add_cat_query = "INSERT INTO category_links SET ref_key = $newId, table_id = get_table_id('store_info'), category_id =". $data['store_category'];
					$q_add_cat_query = $this->db->query($add_cat_query);
		 		}


		 		// Adding Store Tags
		 		// echo "Tags Counter" . count($data['store_tags']);

		 		// if(is_array($data['store_tags'])){
		 		// 	foreach ($data['store_tags'] as $key => $tag) {
		 		// 		$add_tags_query = "INSERT INTO tag_links SET ref_key = $newId, table_id = get_table_id('store_info'), tag_id = $tag";
				// 		$q_add_tags_query = $this->db->query($add_tags_query);	
		 		// 	}

		 		// }else{
		 		// 		$add_tags_query = "INSERT INTO tag_links SET ref_key = $newId, table_id = get_table_id('store_info'), tag_id =". $data['store_tags'];
				// 		$q_add_tags_query = $this->db->query($add_tags_query);	
		 		// }


		 		//Adding Store Hours

		 		// $sdata = array(
				// 				'ref_key' 	=> $newId,
				// 				'table_id' 	=> "get_table_id('store_info')",
				// 				'sub_id' 	=> $sub_id,
				// 				'is_active' => TRUE,
				// 			);

		 		// $add_sched_query = "INSERT INTO schedule_info SET ref_key = $newId, table_id = get_table_id('store_info'), sub_id = $sub_id, is_active = 1";
				// $q_add_sched_query = $this->db->query($add_sched_query);

				// if($q_add_sched_query){
				// 	$schedId = $this->db->insert_id();
				// 	if(is_array($data['store_trading_hours_custom'])){
				// 		foreach ($data['store_trading_hours_custom'] as $key => $tradehours) {

			 	// 			$add_hrs_query = "INSERT INTO schedule_day SET schedule_id = $schedId, day_id = get_day_id('".$tradehours['day']."'), time_start = '".$tradehours['opening_time']."', time_end = '".$tradehours['closing_time']."'";
				// 			$q_add_hrs_query = $this->db->query($add_hrs_query);
				// 		}	
				// 	}
				// }

				return 'Store successfully Added!';
		}else{
		 	return NULL;
		}
	}
	
	public function update_store($id, $data, $sub_id=1){

    $infodata = array(
			'store_name' 		=> $data['store_name'],
			'contact_number' 	=> $data['store_contact_number'],
			'email_address' 	=> $data['store_email_address'],
			'website' 			=> $data['store_website'],
			'description' 		=> $data['store_description'],
			'store_trading_hours' 		=> $data['store_trading_hours'],
			'store_trading_hours_daily' 		=> json_encode($data['store_trading_hours_daily']),
			'store_trading_hours_custom' 		=> json_encode($data['store_trading_hours_custom']),
			'store_tags' 		=> json_encode($data['store_tags']),
			'owner_name'	 	=> $data['owner_name'],
			'owner_email' 		=> $data['owner_email'],
			'owner_contact_number' => $data['owner_contact_number'],
			'is_active' 		=> $data['is_active'],
			'is_featured' 		=> $data['store_featured'],
			'eventbrite_id' 		=> $data['eventbrite_id'],
			'has_media' 		=> 1,
			'sub_id' 			=> $sub_id
		);

		$this->db->where('store_id',$id);
		if($this->db->update('store_info',$infodata)){

      // Upload file
       $uploaddata = $this->upload($id, 'uploads/stores/');

       if($uploaddata){
         $updateimagedata = array(
			      'store_image' 		=> 'uploads/stores/' . $uploaddata['upload_data']['file_name']
         );

         $this->db->where('store_id',$id);
         if($this->db->update('store_info',$updateimagedata)){
         }
       }

       // Update Store Categories
		 		if(is_array($data['store_category'])){
          
          $remove_cat_query = "DELETE  FROM category_links WHERE ref_key = $id AND table_id = get_table_id('store_info')";
					$q_add_cat_query = $this->db->query($remove_cat_query);

		 			foreach ($data['store_category'] as $key => $category) {
						$add_cat_query = "INSERT INTO category_links SET ref_key = $id, table_id = get_table_id('store_info'), category_id = $category";
						$q_add_cat_query = $this->db->query($add_cat_query);
		 			}

		 		}else{
					
					$remove_cat_query = "DELETE  FROM category_links WHERE ref_key = $id AND table_id = get_table_id('store_info')";
					$q_add_cat_query = $this->db->query($remove_cat_query);
					$add_cat_query = "INSERT INTO category_links SET ref_key = $id, table_id = get_table_id('store_info'), category_id =". $data['store_category'];
					$q_add_cat_query = $this->db->query($add_cat_query);
		 		}

      return 'Store successfully Updated';
    }
	}	
	
	public function delete_store($id, $stat = null){
		// $this->db->delete('tablename', array('ID' => $id));
    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('store_id',$id);
		if($this->db->update('store_info',$infodata)){
      return 'Store successfully Archived';
    }
	}

//============================= CATEGORIES DB QUERY ==========================================================//

	public function get_categories($id=NULL, $where=NULL, $sort=NULL, $limit = 100, $skip = 0, $sub_id=1){
		if($id === NULL){

      $where_s = "";
      if($where){

        // echo $where;
        // exit;

        $whereparams = json_decode($where);

        // var_dump($whereparams);
        // exit;

        if($whereparams){
          foreach($whereparams as $paramkey=>$param){
            foreach($param as $key=>$value){
              $where_s .= " AND " . $paramkey . " " . $key . " '" . $value . "'";
            }
          }
        }
        // var_dump($whereparams);
        // echo $where_s;
        // exit;
      }

      $sql_info_count = "SELECT COUNT(*) as c from compassdb.categories where sub_id = $sub_id AND is_active = 1 " . (($where) ? $where_s : "");
			$q_info_count = $this->db->query($sql_info_count);

			$sql = "SELECT c.category_id, c.item_name AS category_name, c.description as category_description, c.has_media, date_created, category_image, category_logo FROM compassdb.categories c WHERE sub_id = $sub_id AND is_active = 1 " . (($where) ? $where_s : "") . (($sort) ? "ORDER BY $sort" : "") . " LIMIT $limit OFFSET $skip";
			$q = $this->db->query($sql);

      // echo $sql;
      // exit;

			if($q->num_rows() > 0){
				foreach($q->result_array() as $row){
					$data[] = $row;
				}

        return array('total_rows' => $q_info_count->result_array()[0]['c'], 'categories' => $data);
				// return $data;
			}else{
				return NULL;
			}

		}else{
			$sql2 = "SELECT c.category_id, c.item_name AS category_name, c.description as category_description, c.has_media, date_created, category_image, category_logo FROM compassdb.categories c WHERE c.category_id=$id AND sub_id = $sub_id AND is_active = 1";
			$q2 = $this->db->query($sql2);

			if($q2->num_rows() > 0){
				foreach($q2->result_array() as $key => $row){
					if($key == 0){
						return $row;
					}
				}
			}else{
				return NULL;
			}	
		}
	}
	
	public function add_category($data, $sub_id=1){
			$cdata = array(
				  'item_name' 	=> $data['category_name'],
					'description' 	=> $data['description'],
          'is_active' 		=> $data['is_active'],
					'has_media' 	=> 1,
					'sub_id' 		=> $sub_id
				);

		 if($this->db->insert('categories',$cdata)){	

       $newId = $this->db->insert_id();

       // Upload file
       $uploaddata = $this->upload($newId, 'uploads/categories/');

       if($uploaddata){
         $updateimagedata = array(
			      'category_image' 		=> 'uploads/categories/' . $uploaddata['upload_data']['file_name']
         );

         $this->db->where('category_id',$newId);
         if($this->db->update('categories',$updateimagedata)){
         }
       }

       // Upload file
       $uploaddata2 = $this->upload2($newId.'_logo', 'uploads/categories/');

       if($uploaddata2){
         $updateimagedata2 = array(
			      'category_logo' 		=> 'uploads/categories/' . $uploaddata2['upload_data']['file_name']
         );

         $this->db->where('category_id',$newId);
         if($this->db->update('categories',$updateimagedata2)){
         }
       }

		 	 return 'Category successfully Added!';
		 }else{
		 	return NULL;
		 }	
	}
	
	public function update_category($id, $data, $sub_id = 1){
			$cdata = array(
				    'item_name' 	=> $data['category_name'],
					'description' 	=> $data['description'],
          'is_active' 		=> $data['is_active'],
					'has_media' 	=> 1,
					'sub_id' 		=> $sub_id
				);		

		$this->db->where('category_id',$id);
		if($this->db->update('categories',$cdata)){
      // Upload file
       $uploaddata = $this->upload($id, 'uploads/categories/');

       if($uploaddata){
         $updateimagedata = array(
			      'category_image' 		=> 'uploads/categories/' . $uploaddata['upload_data']['file_name']
         );

         $this->db->where('category_id',$id);
         if($this->db->update('categories',$updateimagedata)){
         }
       }

       // Upload file
       $uploaddata2 = $this->upload2($id.'_logo', 'uploads/categories/');

       if($uploaddata2){
         $updateimagedata2 = array(
			      'category_logo' 		=> 'uploads/categories/' . $uploaddata2['upload_data']['file_name']
         );

         $this->db->where('category_id',$id);
         if($this->db->update('categories',$updateimagedata2)){
         }
       }

       return 'Category successfully Updated!';
    } else {
		  return NULL;
    }
	}	
	
	public function delete_category($id, $stat = null){
		// return NULL;
		//$this->db->delete('tablename', array('ID' => $id));
    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('category_id',$id);
		if($this->db->update('categories',$infodata)){
      return 'Category successfully Archived';
    }
	}

//============================= FLOORPLANS DB QUERY ==========================================================//

	public function get_floorplans($id=NULL, $limit = 100, $skip = 0, $sub_id=1){
		if($sub_id === NULL){
			return NULL;
		}else{

			if($id === NULL){

        $sql_info_count = "SELECT COUNT(*) as c from compassdb.floorplans where sub_id = $sub_id AND is_active = 1";
			  $q_info_count = $this->db->query($sql_info_count);

				$sql = "SELECT floorplan_id, floorplan_name, is_active, has_media, level as floorplan_level, floorplan_file FROM floorplans WHERE sub_id = $sub_id AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $row){
						$data[] = $row;
					}

          return array('total_rows' => $q_info_count->result_array()[0]['c'], 'floorplans' => $data);
					// return $data;
				}else{
					return NULL;
				}

			}else{
				$sql = "SELECT floorplan_id, floorplan_name, is_active, has_media, level as floorplan_level FROM floorplans WHERE floorplan_id=$id AND sub_id = $sub_id AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $key => $row){
						if($key == 0){
							return $row;
						}
					}
				}else{
					return NULL;
				}	
			}
		}	
	}
	
	public function add_floorplan($data, $sub_id=1){
		if($sub_id === NULL){
			return NULL;
		}else{		
			$fdata = array(
				    'floorplan_name' => $data['floorplan_name'],
				    'is_active' 	 => $data['is_active'],
				    'has_media' 	=> 1,
				    'level' 	 	 => $data['floorplan_level'],
					'sub_id' 	 	 => $sub_id
				);

			 if($this->db->insert('floorplans',$fdata)){	
         $newId = $this->db->insert_id();

          // Upload file
          $uploaddata = $this->upload($newId, 'uploads/floorplans/');

          if($uploaddata){
            $updateimagedata = array(
                'floorplan_file' 		=> 'uploads/floorplans/' . $uploaddata['upload_data']['file_name']
            );

            $this->db->where('floorplan_id',$newId);
            if($this->db->update('floorplans',$updateimagedata)){
            }
          }

			 	  return 'Floorplan successfully Added!';
			 }else{
			 	return NULL;
			 }
		}
	}
	
	public function update_floorplan($id, $data, $sub_id = 1){

		$fdata = array(
			    'floorplan_name' => $data['floorplan_name'],
			    'is_active' 	 => $data['is_active'],
			    'has_media' 	=> 1,
			    'level' 	 	 => $data['floorplan_level'],
          'sub_id' 	 	 => $sub_id
			);
		
		$this->db->where('floorplan_id',$id);
		
		if($this->db->update('floorplans',$fdata)){	
      // Upload file
      //  $uploaddata = $this->upload($id, 'uploads/floorplans/');

      //  if($uploaddata){
      //    $updateimagedata = array(
			//       'floorplan_file' 		=> 'uploads/floorplans/' . $uploaddata['upload_data']['file_name']
      //    );

      //    $this->db->where('floorplan_id',$id);
      //    if($this->db->update('floorplans',$updateimagedata)){
      //    }
      //  }
		 	return 'Floorplan successfully Updated!';
		}else{
		 	return NULL;
		}
		
	}	
	
	public function delete_floorplan($id, $stat = null){
		// return NULL;
		//$this->db->delete('tablename', array('ID' => $id));
    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('floorplan_id',$id);
		if($this->db->update('floorplans',$infodata)){

      $roomdata = array(
        'is_active' 		=> 0
      );
      $this->db->where('floor_plan_id',$id);
      if($this->db->update('rooms',$infodata)){
        return 'Floorplan and store assignments successfully Archived';
      }

    }
	}

//============================= AMENITIES DB QUERY ==========================================================//

	public function get_amenities($id=NULL, $sub_id=1){
		if($sub_id === NULL){
			return NULL;
		}else{

			if($id === NULL){

        $sql_info_count = "SELECT COUNT(*) as c from map_item where sub_id = $sub_id AND is_active = 1 AND item_type = 'a'";
			  $q_info_count = $this->db->query($sql_info_count);

				$sql = "SELECT item_id as amenity_id, item_name as amenity_name, description as amenity_decription, is_active, has_media, item_image FROM map_item where sub_id = $sub_id AND is_active = 1 AND item_type = 'a'";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $row){
						$data[] = $row;
					}

          return array('total_rows' => $q_info_count->result_array()[0]['c'], 'amenities' => $data);
					// return $data;
				}else{
					return 0;
				}

			}else{
        
				$sql = "SELECT item_id as amenity_id, item_name AS amenity, description as amenity_description, is_active, has_media, item_image FROM map_item where sub_id = $sub_id AND item_type = 'a' AND item_id = $id";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $key => $row){
						if($key == 0){
							return $row;
						}
					}
				}else{
					return NULL;
				}	
			}
		}
	}
	
	public function add_amenity($data, $sub_id=1){
		if($sub_id === NULL){
			return NULL;
		}else{		
			$adata = array(
				    'item_name' 	=> $data['amenity_name'],
				    'item_type'		=> 'a',
				    'is_active' 	=> 1,
				    'has_media' 	=> FALSE,
				    'description' 	=> $data['amenity_description'],
					'sub_id' 	 	=> $sub_id
				);

			 if($this->db->insert('map_item',$adata)){	
			 	$newId = $this->db->insert_id();

        // Upload file
        $uploaddata = $this->upload($newId, 'uploads/amenities/');

        if($uploaddata){
          $updateimagedata = array(
              'item_image' 		=> 'uploads/amenities/' . $uploaddata['upload_data']['file_name']
          );

          $this->db->where('item_id',$newId);
          if($this->db->update('map_item',$updateimagedata)){
          }
        }

        return 'Amenity successfully Added!';
			 }else{
			 	return NULL;
			 }
		}
	}
	
	public function update_amenity($id, $data, $sub_id=1){
			$fdata = array(
			    'item_name' 	=> $data['amenity_name'],
          'item_type'		=> 'a',
          'is_active' 	=> 1,
          'has_media' 	=> FALSE,
          'description' 	=> $data['amenity_description'],
					'sub_id' 	 	=> $sub_id
			);
		
		$this->db->where('item_id',$id);
		
		if($this->db->update('map_item',$fdata)){	
      // Upload file
       $uploaddata = $this->upload($id, 'uploads/amenities/');

       if($uploaddata){
         $updateimagedata = array(
			      'item_image' 		=> 'uploads/amenities/' . $uploaddata['upload_data']['file_name']
         );

         $this->db->where('item_id',$id);
         if($this->db->update('map_item',$updateimagedata)){
         }
       }
		 	return 'Amenity successfully Updated!';
		}else{
		 	return NULL;
		}
	}	
	
	public function delete_amenity($id, $stat = null){
		// return NULL;
		//$this->db->delete('tablename', array('ID' => $id));
    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('item_id',$id);
		if($this->db->update('map_item',$infodata)){
      return 'Amenity successfully Archived';
    }
	}		

  

//============================= KIOSKS DB QUERY ==========================================================//

	public function get_kiosks($id = null, $sub_id = 1){
		if($sub_id === NULL){
			return NULL;
		}else{

			if($id === NULL){
				$sql = "SELECT item_id as kiosk_id, item_name AS kiosk_name, description AS kiosk_description, is_active, has_media FROM map_item where sub_id = $sub_id AND item_type = 'k' AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $row){
						$data[] = $row;
					}
					return array('kiosks' => $data);
				}else{
					return 0;
				}

			}else{
				$sql = "SELECT item_id as kiosk_id, item_name AS kiosk_name, description AS kiosk_description, is_active, has_media FROM map_item where sub_id = $sub_id AND item_type = 'k' AND item_id = $id";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $key => $row){
						if($key == 0){
							return $row;
						}
					}
				}else{
					return NULL;
				}	
			}
		}
	}
	
	public function add_kiosk($data, $sub_id = 1){
		if($sub_id === NULL){
			return NULL;
		}else{		
			$kdata = array(
				    'item_name' 	=> $data['kiosk_name'],
				    'item_type' 	=> 'k',
				    'is_active' 	=> 1,
				    'has_media' 	=> FALSE,
				    'description' 	=> $data['kiosk_description'],
					'sub_id' 	 	=> $sub_id
				);

        // var_dump($kdata);
        // exit;

			 if($this->db->insert('map_item',$kdata)){	
			 	$newId = $this->db->insert_id();

          return 'Kiosk successfully Added!';

			 }else{
			 	return NULL;
			 }
		}
	}
	
	public function update_kiosk($id, $data, $sub_id = 1){
			$adata = array(
			    'item_name' 	 => $data['kiosk_name'],
			    'is_active' 	 => 1,
			    'has_media' 	 => FALSE,
			    'description' 	 => $data['kiosk_description'],
				'sub_id' 	 	 => $sub_id
			);

			 $this->db->where('item_id',$id);
		
		if($this->db->update('map_item',$adata)){	

		 	return 'Kiosk successfully Updated!';
		}else{
		 	return NULL;
		}
	}	
	
	public function delete_kiosk($id, $stat = null){
		// return NULL;
		//$this->db->delete('tablename', array('ID' => $id));
    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('item_id',$id);
		if($this->db->update('map_item',$infodata)){
      return 'Kiosk successfully Archived';
    }
	}


//============================= STOREASSIGNMENTS DB QUERY ==========================================================//

	public function get_storeassignments($id=NULL, $limit = 100, $skip = 0, $sub_id=1){
    if($sub_id === NULL){
			return NULL;
		}else{

			if($id === NULL){

        $sql_info_count = "SELECT COUNT(*) as c from compassdb.rooms where sub_id = $sub_id AND is_active = 1";
			  $q_info_count = $this->db->query($sql_info_count);

				$sql = "SELECT room_id, floor_plan_id, store_id, room_name FROM rooms WHERE sub_id = $sub_id AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $row){

            $store = $this->get_stores($row['store_id']);
            $row['store'] = $store;
						$data[] = $row;
					}

          return array('total_rows' => $q_info_count->result_array()[0]['c'], 'storeassignments' => $data);
					// return $data;
				}else{
					return NULL;
				}

			}else{
				$sql = "SELECT room_id, floor_plan_id, store_id, room_name FROM rooms WHERE sub_id = $sub_id AND room_id=$id AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $key => $row){
						if($key == 0){
              $store = $this->get_stores($row['store_id']);
              $row['store'] = $store;
              $floorplan = $this->get_floorplans($row['floor_plan_id']);
              $row['floorplan'] = $floorplan;
							return $row;
						}
					}
				}else{
					return NULL;
				}	
			}
		}	
	}
	
	public function add_storeassignment($data, $sub_id=1){
    if($sub_id === NULL){
			return NULL;
		}else{		

			$fdata = array(
				    'floor_plan_id' => $data['floor_plan_id'],
				    'store_id' => $data['store_id'],
				    'room_name' => $data['room_name'],
				    'is_active' 	 => $data['is_active'],
					  'sub_id' 	 	 => $sub_id
				);

			 if($this->db->insert('rooms',$fdata)){	
         $newId = $this->db->insert_id();

			 	  return 'Storeassignment successfully Added!';
			 }else{
			 	return NULL;
			 }
		}
	}
	
	public function update_storeassignment($id, $data, $sub_id=1){
		// $this->db->where('ID',$id);
		// $this->db->update('tablename',$data);
    $fdata = array(
				    'floor_plan_id' => $data['floor_plan_id'],
				    'store_id' => $data['store_id'],
				    'room_name' => $data['room_name'],
				    'is_active' 	 => $data['is_active'],
					  'sub_id' 	 	 => $sub_id
				);

        $this->db->where('room_id',$id);

        if($this->db->update('rooms',$fdata)){
         $newId = $this->db->insert_id();

			 	  return 'Storeassignment successfully Updated!';
			 }else{
			 	return NULL;
			 }

	}	
	
	public function delete_storeassignment($id, $stat = null){

    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('room_id',$id);
		if($this->db->update('rooms',$infodata)){
      return 'Storeassignment successfully Archived';
    }
	}

//============================= KIOSKSASSIGMENTS DB QUERY ==========================================================//

	public function get_kioskassignments($id=NULL, $limit = 100, $skip = 0, $sub_id=1){
    if($id === NULL){

        $sql_info_count = "SELECT COUNT(*) c FROM `map_item_location` WHERE table_id = 8 AND ref_key IN (SELECT item_id FROM `map_item` WHERE is_active = 1 AND sub_id = 1 AND item_type = 'k') AND is_active = 1";
			  $q_info_count = $this->db->query($sql_info_count);

				$sql = "SELECT * FROM `map_item_location` WHERE table_id = 8 AND ref_key IN (SELECT item_id FROM `map_item` WHERE is_active = 1 AND sub_id = 1 AND item_type = 'k') AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $row){

            $kiosk = $this->get_kiosks($row['ref_key']);
            $row['kiosk'] = $kiosk;
						$data[] = $row;
					}

          return array('total_rows' => $q_info_count->result_array()[0]['c'], 'kioskassignments' => $data);
					// return $data;
				}else{
					return NULL;
				}

			}else{
				$sql = "SELECT * FROM `map_item_location` WHERE table_id = 8 AND mil_id = $id AND ref_key IN (SELECT item_id FROM `map_item` WHERE is_active = 1 AND sub_id = 1 AND item_type = 'k')";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $key => $row){
            $kiosk = $this->get_kiosks($row['ref_key']);
            $row['kiosk'] = $kiosk;
						if($key == 0){
							return $row;
						}
					}
				}else{
					return NULL;
				}	
			}
	}
	
	public function add_kioskassignment($data, $sub_id=1){
		 if($sub_id === NULL){
			return NULL;
		}else{		

			$fdata = array(
				    'ref_key' => $data['kiosk_id'],
				    'table_id' =>8,
				    'room_id' => null,
				    'coordinate_x' 	 => $data['coordinate_x'],
				    'coordinate_y' 	 => $data['coordinate_y'],
				    'floorplan_id' 	 => $data['floor_plan_id'],
				    'is_active' 	 => 1,
					  'sub_id' 	 	 => $sub_id
				);

			 if($this->db->insert('map_item_location',$fdata)){	
         $newId = $this->db->insert_id();

			 	  return 'Kiosk assignment successfully Added!';
			 }else{
			 	return NULL;
			 }
		}
	}
	
	public function update_kioskassignment($id, $data, $sub_id=1){
		$fdata = array(
				    'ref_key' => $data['kiosk_id'],
				    'table_id' =>8,
				    'room_id' => null,
				    'coordinate_x' 	 => $data['coordinate_x'],
				    'coordinate_y' 	 => $data['coordinate_y'],
				    'floorplan_id' 	 => $data['floor_plan_id'],
				    'is_active' 	 => 1,
					  'sub_id' 	 	 => $sub_id
				);

        $this->db->where('mil_id',$id);

			 if($this->db->update('map_item_location',$fdata)){	
         $newId = $this->db->insert_id();

			 	  return 'Kiosk assignment successfully Updated!';
			 }
	}	
	
	public function delete_kioskassignment($id, $stat=null){
		$infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('mil_id',$id);
		if($this->db->update('map_item_location',$infodata)){
      return 'Kioskassignment successfully Archived';
    }
	}	

  //============================= KIOSKSASSIGMENTS DB QUERY ==========================================================//

	public function get_amenitiesassignments($id=NULL, $limit = 100, $skip = 0, $sub_id=1){
    if($id === NULL){

        $sql_info_count = "SELECT COUNT(*) c FROM `map_item_location` WHERE table_id = 8 AND ref_key IN (SELECT item_id FROM `map_item` WHERE is_active = 1 AND sub_id = 1 AND item_type = 'a') AND is_active = 1";
			  $q_info_count = $this->db->query($sql_info_count);

				$sql = "SELECT * FROM `map_item_location` WHERE table_id = 8 AND ref_key IN (SELECT item_id FROM `map_item` WHERE is_active = 1 AND sub_id = 1 AND item_type = 'a') AND is_active = 1";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $row){

            $amenity = $this->get_amenities($row['ref_key']);
            $row['amenity'] = $amenity;
						$data[] = $row;
					}

          return array('total_rows' => $q_info_count->result_array()[0]['c'], 'amenitiesassignments' => $data);
					// return $data;
				}else{
					return NULL;
				}

			}else{
				$sql = "SELECT * FROM `map_item_location` WHERE table_id = 8 AND mil_id = $id AND ref_key IN (SELECT item_id FROM `map_item` WHERE is_active = 1 AND sub_id = 1 AND item_type = 'a')";
				$q = $this->db->query($sql);

				if($q->num_rows() > 0){
					foreach($q->result_array() as $key => $row){
            $amenity = $this->get_amenities($row['ref_key']);
            $row['amenity'] = $amenity;
						if($key == 0){
							return $row;
						}
					}
				}else{
					return NULL;
				}	
			}
	}
	
	public function add_amenityassignment($data, $sub_id=1){
    if($sub_id === NULL){
			return NULL;
		}else{		

			$fdata = array(
				    'ref_key' => $data['amenity_id'],
				    'table_id' =>8,
				    'room_id' => null,
				    'coordinate_x' 	 => $data['coordinate_x'],
				    'coordinate_y' 	 => $data['coordinate_y'],
				    'floorplan_id' 	 => $data['floor_plan_id'],
				    'is_active' 	 => 1,
					  'sub_id' 	 	 => $sub_id
				);

			 if($this->db->insert('map_item_location',$fdata)){	
         $newId = $this->db->insert_id();

			 	  return 'Amenity assignment successfully Added!';
			 }else{
			 	return NULL;
			 }
		}
	}
	
	public function update_amenityassignment($id, $data, $sub_id=1){

			$fdata = array(
				    'ref_key' => $data['amenity_id'],
				    'table_id' =>8,
				    'room_id' => null,
				    'coordinate_x' 	 => $data['coordinate_x'],
				    'coordinate_y' 	 => $data['coordinate_y'],
				    'floorplan_id' 	 => $data['floor_plan_id'],
				    'is_active' 	 => 1,
					  'sub_id' 	 	 => $sub_id
				);

        $this->db->where('mil_id',$id);

			 if($this->db->update('map_item_location',$fdata)){	
         $newId = $this->db->insert_id();

			 	  return 'Amenity assignment successfully Updated!';
			 }

	}	
	
	public function delete_amenityassignment($id, $stat=null){
    $infodata = array(
      'is_active' 		=> 0
    );
    $this->db->where('mil_id',$id);
		if($this->db->update('map_item_location',$infodata)){
      return 'Storeassignment successfully Archived';
    }
	}	

}	