<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

class Rest_api extends REST_Controller {

    function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }

        // Construct the parent class
        
        parent::__construct();

        $this->load->model('api_model');

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        // $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['users_delete']['limit'] = 100; // 100 requests per hour per user/key
    }

    // ------------------ USERS ENDPOINTS ------------------------- // 

    public function users_get()
    {

        $id = $this->get('id');
        $sub_id = $this->get('sid');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $users = $this->api_model->get_users();
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }else{

        // Find and return a single record for a particular user.

            $id = (int) $id;

            // Validate the id.
            if ($id <= 0)
            {
                // Invalid id, set the response and exit.
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }else{

                $user = $this->api_model->get_users($id);

                if (!empty($user))
                {
                    $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
                else
                {
                    $this->set_response([
                        'status' => FALSE,
                        'message' => 'User could not be found'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
        }    
    }

    public function users_post()
    {
        // $this->some_model->update_user( ... );
        $id = $this->get('id');

        $userdata = $this->input->post(); //revise after defining the POST data

        if ($id === NULL){
            $message = $this->api_model->add_user($userdata);
        }elseif ((int)$id <= 0) {
             $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }else{
            $message = $this->api_model->update_user($id, $userdata);
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function users_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $this->

        $message = $message = $this->api_model->delete_user($id);

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }


    // ------------------ STORES ENDPOINTS ------------------------- // 

    public function stores_get($id = NULL)
    {
       
    	$limit = $this->get('limit') ? $this->get('limit') : 10 ;
      $skip = $this->get('skip') ? $this->get('skip') : 0 ;
      $where = $this->get('where') ? $this->get('where') : '';
      $sort = $this->get('sort') ?  $this->get('sort') : '';


    	// Stores from a data store e.g. database

      //   $id = $this->get('id');
        
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {

            $stores = $this->api_model->get_stores(null, $where, $sort, $limit, $skip);


            // Check if the users data store contains stores (in case the database result returns NULL)
            if ($stores)
            {
                // Set the response and exit
                $this->response($stores, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response($stores, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                // $this->response([
                //     'status' => FALSE,
                //     'message' => 'No stores were found'
                // ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }else{

            // Find and return a single record for a particular user.

            $id = (int) $id;

            // Validate the id.
            if ($id <= 0)
            {
                // Invalid id, set the response and exit.
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
            }

            // Get the user from the array, using the id as key for retrieval.
            // Usually a model is to be used for this.

            $store = $this->api_model->get_stores($id);

            if (!empty($store))
            {
                $this->set_response($store, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'Store could not be found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function stores_post($id = NULL)
    {

        // $id = $this->get('id');
        $storedata = json_decode($this->input->post('data'), true);


        if ($id === NULL){
            $message = $this->api_model->add_store($storedata);
        }else if ((int)$id >= 0) {
            $message = $this->api_model->update_store($id, $storedata);
        }else{
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function stores_delete($id = NULL)
    {
        // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_store($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }


    // ------------------ CATEGORIES ENDPOINTS ------------------------- // 

    public function categories_get($id = NULL)
    {
       
        $limit = $this->get('limit') ? $this->get('limit') : 10 ;
        $skip = $this->get('skip') ? $this->get('skip') : 0 ;
        $where = $this->get('where') ? $this->get('where') : '';
        $sort = $this->get('sort') ?  $this->get('sort') : '';

        // categories from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {

            $categories = $this->api_model->get_categories(null, $where, $sort, $limit, $skip);
            // Check if the users data store contains categories (in case the database result returns NULL)
            if ($categories)
            {
                // Set the response and exit
                $this->response($categories, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No categories were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

          // Find and return a single record for a particular user.

          $id = (int) $id;

          // Validate the id.
          if ($id <= 0)
          {
              // Invalid id, set the response and exit.
              $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
          }

          // Get the user from the array, using the id as key for retrieval.
          // Usually a model is to be used for this.

          $category = $this->api_model->get_categories($id);

          if (!empty($category))
          {
              $this->set_response($category, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
          }
          else
          {
              $this->set_response([
                  'status' => FALSE,
                  'message' => 'Category could not be found'
              ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
          }
        }

    }

    public function categories_post($id = NULL)
    {

        // $id = $this->get('id');
        $categorydata = json_decode($this->input->post('data'), true);

        // $categorydata = $this->input->post();

        if ($id === NULL){
            $message = $this->api_model->add_category($categorydata);
        }elseif ((int)$id <= 0) {
             $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }else{
            $message = $this->api_model->update_category($id, $categorydata);
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function categories_delete($id = NULL)
    {
        // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_category($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }


    // ------------------ FLOORPLANS ENDPOINTS ------------------------- // 

    public function floorplans_get($id = NULL)
    {
       
        $limit = $this->get('limit');
        $skip = $this->get('skip');
        $sort = $this->get('sort');
        $where = $this->get('where');

        // floorplans from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $floorplans = $this->api_model->get_floorplans(null, $limit, $skip);
            // Check if the users data store contains floorplans (in case the database result returns NULL)
            if ($floorplans)
            {
                // Set the response and exit
                $this->response($floorplans, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No floorplans were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $floorplan = $this->api_model->get_floorplans($id);
        if (!empty($floorplan))
        {
            $this->set_response($floorplan, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'floorplan could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function floorplans_post($id = NULL)
    {

        // $id = $this->get('id');
        $floorplandata = json_decode($this->input->post('data'), true);

        // $floorplandata = $this->input->post();

        if ($id === NULL){
            $message = $this->api_model->add_floorplan($floorplandata);
        }elseif ((int)$id <= 0) {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }else{
            $message = $this->api_model->update_floorplan($id, $floorplandata);
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function floorplans_delete($id = NULL)
    {
        // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_floorplan($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }


    // ------------------ AMENITIES ENDPOINTS ------------------------- // 

    public function amenities_get($id = NULL)
    {
       
        $limit = $this->get('limit');
        $skip = $this->get('skip');
        $sort = $this->get('sort');
        $where = $this->get('where');

        // amenities from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $amenities =  $message = $this->api_model->get_amenities();
            // Check if the users data store contains amenities (in case the database result returns NULL)
            if ($amenities)
            {
                // Set the response and exit
                $this->response($amenities, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No amenities were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $amenity = $this->api_model->get_amenities($id);

        if (!empty($amenity))
        {
            $this->set_response($amenity, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'amenity could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function amenities_post($id=null)
    {

        // $id = $this->get('id');
        $amenitydata = json_decode($this->input->post('data'), true);

        // $amenitydata = $this->input->post();

        if ($id === NULL){
            $message = $this->api_model->add_amenity($amenitydata);
        }elseif ((int)$id <= 0) {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }else{
            $message = $this->api_model->update_amenity($id, $amenitydata);
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function amenities_delete($id = null)
    {
        // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_amenity($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }

    // ------------------ KIOSKS ENDPOINTS ------------------------- // 

    public function kiosks_get($id=null)
    {
       
        $limit = $this->get('limit');
        $skip = $this->get('skip');
        $sort = $this->get('sort');
        $where = $this->get('where');

        // kiosks from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $kiosks = $this->api_model->get_kiosks();
            // Check if the users data store contains kiosks (in case the database result returns NULL)
            if ($kiosks)
            {
                // Set the response and exit
                $this->response($kiosks, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No kiosks were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $kiosk = $this->api_model->get_kiosks($id);

        if (!empty($kiosk))
        {
            $this->set_response($kiosk, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'kiosk could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function kiosks_post($id = null)
    {

        // $id = $this->get('id');

        // $kioskdata = $this->input->post();
        $kioskdata = json_decode($this->input->post('data'), true);

        if ($id === NULL){
            $message = $this->api_model->add_kiosk($kioskdata);
        }elseif ((int)$id <= 0) {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code            
        }else{
            $message = $this->api_model->update_kiosk($id, $kioskdata);
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function kiosks_delete($id = null)
    {
        // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_kiosk($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }


    // ------------------ STOREASSIGNMENTS ENDPOINTS ------------------------- // 

    public function storeassignments_get($id = NULL)
    {
       
        $limit = $this->get('limit');
        $skip = $this->get('skip');
        $sort = $this->get('sort');
        $where = $this->get('where');

        // storeassignments from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $storeassignments = $this->api_model->get_storeassignments();
            // Check if the users data store contains storeassignments (in case the database result returns NULL)
            if ($storeassignments)
            {
                // Set the response and exit
                $this->response($storeassignments, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No storeassignments were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $storeassignment = $this->api_model->get_storeassignments($id);

        if (!empty($storeassignment))
        {
            $this->set_response($storeassignment, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'storeassignment could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function storeassignments_post($id = NULL)
    {

        // $id = $this->get('id');
        $storeassignmentdata = json_decode($this->input->post('data'), true);

        // $storeassignmentdata = $this->input->post();

        if ($id === NULL){
            $message = $this->api_model->add_storeassignment($storeassignmentdata);
        }elseif ((int)$id > 0) {
          
            $message = $this->api_model->update_storeassignment($id, $storeassignmentdata);
        }else{
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function storeassignments_delete($id = NULL)
    {
        // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_storeassignment($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }

    // ------------------ KIOSKASSIGNMENTS ENDPOINTS ------------------------- // 

    public function kioskassignments_get($id = NULL)
    {
        $limit = $this->get('limit');
        $skip = $this->get('skip');
        $sort = $this->get('sort');
        $where = $this->get('where');

        // storeassignments from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $kioskassignments = $this->api_model->get_kioskassignments();
            // Check if the users data store contains storeassignments (in case the database result returns NULL)
            if ($kioskassignments)
            {
                // Set the response and exit
                $this->response($kioskassignments, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No kioskassignments were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $kioskassignments = $this->api_model->get_kioskassignments($id);

        if (!empty($kioskassignments))
        {
            $this->set_response($kioskassignments, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'kioskassignments could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function kioskassignments_post($id = NULL)
    {
        // $id = $this->get('id');
        $kioskassignment = json_decode($this->input->post('data'), true);

        // $storeassignmentdata = $this->input->post();

        if ($id === NULL){
            $message = $this->api_model->add_kioskassignment($kioskassignment);
        }elseif ((int)$id > 0) {
            $message = $this->api_model->update_kioskassignment($id, $kioskassignment);
        }else{
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function kioskassignments_delete($id = NULL)
    {
      // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_kioskassignment($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }

    // ------------------ AMENITIESASSIGNMENTS ENDPOINTS ------------------------- // 

    public function amenitiesassignments_get($id = NULL)
    {
        $limit = $this->get('limit');
        $skip = $this->get('skip');
        $sort = $this->get('sort');
        $where = $this->get('where');

        // storeassignments from a data store e.g. database

        // $id = $this->get('id');
        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            $amenitiesassignments = $this->api_model->get_amenitiesassignments();
            // Check if the users data store contains storeassignments (in case the database result returns NULL)
            if ($amenitiesassignments)
            {
                // Set the response and exit
                $this->response($amenitiesassignments, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No amenitiesassignments were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $amenitiesassignments = $this->api_model->get_amenitiesassignments($id);

        if (!empty($amenitiesassignments))
        {
            $this->set_response($amenitiesassignments, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'amenitiesassignments could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function amenitiesassignments_post($id = NULL)
    {
        // $id = $this->get('id');
        $amenityassignment = json_decode($this->input->post('data'), true);

        // $storeassignmentdata = $this->input->post();

        if ($id === NULL){
            $message = $this->api_model->add_amenityassignment($amenityassignment);
        }elseif ((int)$id > 0) {
            $message = $this->api_model->update_amenityassignment($id, $amenityassignment);
        }else{
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }    

        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    public function amenitiesassignments_delete($id = NULL)
    {
      // $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = $this->api_model->delete_amenityassignment($id);

        $this->set_response($message, REST_Controller::HTTP_CREATED); // NO_CONTENT (204) being the HTTP response code
    }


}